<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class ArticleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
        foreach (range(1,51) as $index) {
            DB::table('articles')->insert([
                'category' => $faker->randomElement(['Teen Pregnancy', 'Teen sex','Sexual violence','Birth control','sex education','Sexually transmitted Infections']),
                'title' => $faker->sentence(),
                'body' => $faker->paragraph(300),
                'created_at' => \Carbon\Carbon::now(),
        	'Updated_at' => \Carbon\Carbon::now(),

            ]);
        }
    }
}
