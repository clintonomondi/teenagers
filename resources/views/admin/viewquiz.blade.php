@extends('layouts.app')

@section('content')
    @include('includes.message')
    @include('modals.answer')
    @include('sweet::alert')
    <div class="container-fluid">
            <div class="row clearfix">
                
                <div class="col-xs-12 col-sm-12">
                    <div class="card">
                            <div class="header">
                                    <h2>
                                        {{$quiz->title}} <small>{{$quiz->user->name}}  {{$quiz->created_at->diffForHumans()}}</small>
                                    </h2>
                                    <span class="pull-right"><a data-toggle="modal" data-target="#answer" class="btn bg-pink waves-effect">Answer</a></span>
                        <div class="body">
          <p> {!!$quiz->body!!}</p>
    <hr>
    @foreach($quiz->answer as $answer)
                                        <div class="panel panel-default panel-post">
                                            <div class="panel-heading">
                                                <div class="media">
                                                    <div class="media-left">
                                                        <a href="#">
                                                            
                                                        </a>
                                                    </div>
                                                    <div class="media-body">
                                                        <h4 class="media-heading">
                                                            <a href="#">{{ $answer->user }}</a>
                                                        </h4>
                                                       {{$answer->created_at->diffForHumans()}}
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="panel-body">
                                                <div class="post">
                                                    <div class="post-heading">
                                                        <p>{!!$answer->body !!}</p>
                                                    </div>
                                                    <div class="post-content">
                                                        
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        @endforeach
                                        <div class="panel panel-default panel-post">
                                                <div class="panel-heading">
                                        <div class="panel-footer">
                                                <ul>
                                                    <li>
                                                        <a href="#">
                                                            <i class="material-icons">thumb_up</i>
                                                            <span>12 Likes</span>
                                                        </a>
                                                    </li>
                                                    <li>
                                                        <a href="#">
                                                            <i class="material-icons">comment</i>
                                                            <span>{{ $quiz->answer->count()}} Answers</span>
                                                        </a>
                                                    </li>
                                                    <li>
                                                        <a href="#">
                                                            <i class="material-icons">share</i>
                                                            <span>Share</span>
                                                        </a>
                                                    </li>
                                                </ul>
                                               
                                                </div>
                                            </div>
                                        </div>
                                   
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
@endsection
