@extends('layouts.app')

@section('content')
    @include('modals.askquiz')
    @include('includes.message')
    @include('sweet::alert')
    
    <!-- Exportable Table -->
<div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2>
                       QUESTIONS
                    </h2>
                </div>
                <div class="body">
                    <div class="table-responsive">
                        <table class="table table-bordered table-striped table-hover dataTable js-exportable">
                            <thead>
                                <tr>
                                        <th>#</th>
                                        <th>Title</th>
                                        <th>Sender</th>
                                        <th>Posted on</th>
                                        <th>Status</th>
                                        <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                    @if(count($questions)>0)
                    @foreach($questions as $key=>$question)
                    <tr>
                            <td>{{$key+1}}</td>
                            <td>{{$question->title}}</td>
                            <td>{{$question->user->name}}</td>
                            <td>{{$question->created_at->diffForHumans()}}</td>
                            @if(($question->status)=='Pending')
                            <td><p class="text-primary">{{$question->status}}</p></td>
                            @else
                            <td><p>{{$question->status}}</p></td>
                            @endif
                            <td><a class="btn bg-pink waves-effect"  href="{{route('admin.viewquiz',$question->id)}}" >Read</a></td>
                        </tr>
@endforeach
@endif

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- #END# Exportable Table -->
@endsection
