@extends('layouts.app')

@section('content')
    @include('includes.message')
    @include('sweet::alert')
<!-- Exportable Table -->
<div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2>
                       USERS
                    </h2>
                </div>
                <div class="body">
                    <div class="table-responsive">
                        <table class="table table-bordered table-striped table-hover dataTable js-exportable">
                            <thead>
                                <tr>
                                        <th>#</th>
                                        <th>Name</th>
                                        <th>Email</th>
                                        <th>user type</th>
                                        <th>Profile avater</th>
                                        <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                    @if(count($users)>0)
                                    @foreach($users as $key=>$user)
                                <tr>
                                        <td>{{$key+1}}</td>
                                        <td>{{$user->name}}</td>
                                        <td>{{$user->email}}</td>
                                        <td>{{$user->role}}</td>
                                        <td><img src="/storage/avatars/{{ $user->avatar}}" alt="image" width="40" height="40" alt="User"></td>
                                        <td> <a href="{{route('removeuser',$user->id)}}"  class="btn bg-red waves-effect">Remove</a></td>
                                    </tr>
@endforeach
@endif

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- #END# Exportable Table -->

@endsection
