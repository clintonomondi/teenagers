@extends('layouts.app')

@section('content')
    @include('includes.message')
    @include('sweet::alert')
    <!-- Vertical Layout -->
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2>
                        Edit article
                    </h2>
                </div>
                <div class="body">
                <form method="post" action="{{route('updatearticle',$article->id)}}">
                    @csrf
                    <div class="form-group">
                        <label for="email">Title:</label>
                        <input type="text" class="form-control" name="title" value="{{$article->title}}" required>
                    </div>
                    <div class="form-group">
                        <label for="sel1">Select Category:</label>
                        <select class="form-control" name="category" required>
                            <option value="{{$article->category}}">{{$article->category}}</option>
                            <option value="Teen Pregnancy">Teen Pregnancy</option>
                            <option value="Teen sex">Teen sex</option>
                            <option value="Sexual violence">Sexual violence</option>
                            <option value="Birth control">Birth control</option>
                            <option value="sex education">Sex education</option>
                            <option value="Sexually transmitted Infections ">Sexually transmitted Infections</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="comment">Body:</label>
                        <textarea class="form-control" rows="10" id="article-ckeditor" name="body" required>{{$article->body}}</textarea>
                    </div>
                    <br>
                    <button type="submit" class="btn bg-pink waves-effect">Update</button>
                </form>
            </div>
        </div>
    </div>
@endsection
