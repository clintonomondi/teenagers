@extends('layouts.app')

@section('content')
    @include('includes.message')
    @include('sweet::alert')
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2>
                        Articles
                    </h2>
                </div>
                <div class="body">
                    <div class="table-responsive">
                        <table class="table table-bordered table-striped table-hover dataTable js-exportable">
                <thead>
                <th>#</th>
                <th>Category</th>
                <th>Title</th>
                <th>Posted on</th>
                <th></th>

                </thead>
                <tbody>
                @if(count($articles)>0)
                    @foreach($articles as $key=>$article)
                        <tr>
                            <td>{{$key+1}}</td>
                            <td>{{$article->category}}</td>
                            <td>{{$article->title}}</td>
                            <td>{{$article->created_at}}</td>
                            <td>
                                <div class="btn-group" role="group">
                                    <div class="btn-group" role="group">
                                        <button type="button" class="btn bg-pink waves-effect dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            Actions
                                            <span class="caret"></span>
                                        </button>
                                        <ul class="dropdown-menu">
                                            <li><a href="{{route('editarticle',$article->id)}}">Edit</a></li>
                                            <li><a href="{{route('viewarticle',$article->id)}}">View</a></li>
                                            <li><a href="{{route('removearticle',$article->id)}}">Remove</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </td>

                        </tr>

                    @endforeach
                @else
                    <p>No Articles yet</p>
                @endif
            </table>
            {{$articles->links()}}
        </div>
    </div>
            </div>
        </div>
    </div>
@endsection
