@extends('layouts.app')

@section('content')
    @include('includes.message')
    @include('sweet::alert')
    <!-- Vertical Layout -->
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2>
                        Compose article
                    </h2>
                </div>
                <div class="body">
                    <form method="post" action="{{route('postarticle')}}">
                        @csrf
                        <label for="email_address">Title</label>
                        <div class="form-group">
                            <div class="form-line">
                                <input type="text" id="email_address" name="title" class="form-control" placeholder="Enter title" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="email_address">Select Category:</label>
                            <select class="form-control show-tick" name="category" required>
                                <option></option>
                                <option value="Teen Pregnancy">Teen Pregnancy</option>
                                <option value="Teen sex">Teen sex</option>
                                <option value="Sexual violence">Sexual violence</option>
                                <option value="Birth control">Birth control</option>
                                <option value="sex education">Sex education</option>
                                <option value="Sexually transmitted Infections ">Sexually transmitted Infections</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="comment">Body:</label>
                            <textarea class="form-control" rows="30" id="article-ckeditor" name="body" required></textarea>
                        </div>
                        <br>
                        <button type="submit" class="btn bg-pink waves-effect">POST</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- #END# Vertical Layout -->

@endsection
