@extends('layouts.app')

@section('content')
    @include('modals.askquiz')
    @include('includes.message')
    @include('sweet::alert')
    
    <!-- Exportable Table -->
<div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2>
                       MY QUESTIONS
                    </h2>
                    <span class="pull-right"><a data-toggle="modal" data-target="#askquiz" class="btn bg-pink waves-effect">Ask Question</a></span>
                </div>
                <div class="body">
                    <div class="table-responsive">
                        <table class="table table-bordered table-striped table-hover dataTable js-exportable">
                            <thead>
                                <tr>
                                        <th>#</th>
                                        <th>Title</th>
                                        <th>Status</th>
                                        <th>Posted on</th>
                                        <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                    @if(count($questions)>0)
                    @foreach($questions as $key=>$question)
                    <tr>
                            <td>{{$key+1}}</td>
                            <td>{{  $question->title}}</td>
                            <td>{{  $question->status}}</td>
                            <td>{{  $question->created_at->diffForHumans()}}</td>
                            <td>
                                    <div class="btn-group" role="group">
                                        <div class="btn-group" role="group">
                                            <button type="button" class="btn bg-pink waves-effect dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                Actions
                                                <span class="caret"></span>
                                            </button>
                                            <ul class="dropdown-menu">
                                                <li><a href="{{route('user.viewquiz',$question->id)}}">View</a></li>
                                                <li><a href="{{route('cancelquiz',$question->id)}}">Cancel</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </td>
                        </tr>
@endforeach
@endif

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- #END# Exportable Table -->
@endsection
