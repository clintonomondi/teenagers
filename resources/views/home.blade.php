@extends('layouts.app')

@section('content')
    @include('sweet::alert')
@include('includes.message')

@can('isAdmin')
<!-- Widgets -->
<div class="row clearfix">
        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
            <div class="info-box bg-pink hover-expand-effect">
                <div class="icon">
                    <i class="material-icons">playlist_add_check</i>
                </div>
                <div class="content">
                    <div class="text">PENDING QUESTIONS</div>
                    <div class="number count-to" data-from="0" data-to={{$pending}} data-speed="1500" data-fresh-interval="10"></div>
                </div>
            </div>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
            <div class="info-box bg-cyan hover-expand-effect">
                <div class="icon">
                    <i class="material-icons">help</i>
                </div>
                <div class="content">
                    <div class="text">ARTICLES</div>
                    <div class="number count-to" data-from="0" data-to={{$articles}} data-speed="1500" data-fresh-interval="10"></div>
                </div>
            </div>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
            <div class="info-box bg-light-green hover-expand-effect">
                <div class="icon">
                    <i class="material-icons">book</i>
                </div>
                <div class="content">
                    <div class="text">COMMENTS</div>
                    <div class="number count-to" data-from="0" data-to={{ $comments }} data-speed="1000" data-fresh-interval="10"></div>
                </div>
            </div>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
            <div class="info-box bg-orange hover-expand-effect">
                <div class="icon">
                    <i class="material-icons">person_add</i>
                </div>
                <div class="content">
                    <div class="text">USERS</div>
                    <div class="number count-to" data-from="0" data-to={{$users}} data-speed="1500" data-fresh-interval="10"></div>
                </div>
            </div>
        </div>
    </div>
    <!-- #END# Widgets -->
    @endcan

    @can('isUser')
<!-- Widgets -->
<div class="row clearfix">
        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
            <div class="info-box bg-pink hover-expand-effect">
                <div class="icon">
                    <i class="material-icons">playlist_add_check</i>
                </div>
                <div class="content">
                    <div class="text"> MY PENDING QUESTIONS</div>
                    <div class="number count-to" data-from="0" data-to={{$mypending}} data-speed="1500" data-fresh-interval="10"></div>
                </div>
            </div>
        </div>
        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
            <div class="info-box bg-cyan hover-expand-effect">
                <div class="icon">
                    <i class="material-icons">help</i>
                </div>
                <div class="content">
                    <div class="text">ARTICLES</div>
                    <div class="number count-to" data-from="0" data-to={{$articles}} data-speed="1500" data-fresh-interval="10"></div>
                </div>
            </div>
        </div>
        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
            <div class="info-box bg-light-green hover-expand-effect">
                <div class="icon">
                    <i class="material-icons">book</i>
                </div>
                <div class="content">
                    <div class="text">MY COMMENTS</div>
                    <div class="number count-to" data-from="0" data-to={{ $mycomments }} data-speed="1000" data-fresh-interval="10"></div>
                </div>
            </div>
        </div>
    </div>
    <!-- #END# Widgets -->
    @endcan
            <div class="row justify-content-center">
                    <div class="col-sm-12">
                        <div class="card">
                            <div class="header">
                                <h2>
                                    Welcome, {{ Auth::user()->name }} <small>You have logged in as {{ Auth::user()->role }}</small>
                                </h2>
                                <ul class="header-dropdown m-r--5">
                                    
                                </ul>
                            </div>
                            <div class="body">
                              <h4>WHAT TEENAGERS SHOULD KNOW</h4>
                              <p>
                                 Universal access to accurate sexual and reproductive health information;
                                A range of safe and affordable contraceptive methods;
                                Sensitive counselling
                                Quality obstetric and antenatal care for all pregnant women and girls; and
                                The prevention and management of sexually transmitted infections, including HIV
                              <p>
                                    The bad news: Teens are not as invincible as some people think. Although teens might fe el confident
                                    online, they do make mistakes and often give up quickly. Fast-moving teens are also less cautious than
                                    adults and make snap
                                    judgments; these lead to fewer successfully completed tasks.
                              </p>
                            </div>
                        </div>
                    </div>

    </div>

@endsection
