@extends('layouts.app')

@section('content')
    @include('includes.message')
    @if(count($articles)>0)
        @foreach($articles as $article)
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-6 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                                {{$article->title}} <small>{{$article->category}}  {{$article->created_at->diffForHumans()}}</small>
                            </h2>
                            <ul class="header-dropdown m-r--5">
                                <li>
                                    <a href="javascript:void(0);" data-toggle="cardloading" data-loading-effect="timer">
                                        <i class="material-icons">loop</i>
                                    </a>
                                </li>
                                <li class="dropdown">
                                    <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                        <i class="material-icons">more_vert</i>
                                    </a>
                                    {{--<ul class="dropdown-menu pull-right">--}}
                                        {{--<li><a href="{{route('editarticle',$article->id)}}">Edit</a></li>--}}
                                        {{--<li><a href="{{route('articles')}}">Back to articles</a></li>--}}
                                    {{--</ul>--}}
                                </li>
                            </ul>
                        </div>
                        <div class="body">
                            {!!str_limit($article->body,600)!!} <a href="{{route('user.viewarticle',$article->id)}}" class="text-success">..read more..</a>
                        </div>
                        <div class="panel panel-default panel-post">
                            <div class="panel-footer">
                                <ul>
                                    <li>
                                        <a href="#">
                                            <i class="material-icons">thumb_up</i>
                                            <span>12 Likes</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="{{route('user.viewarticle',$article->id)}}">
                                            <i class="material-icons">comment</i>
                                            <span>{{$article->comment()->count()}} Comments</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <!-- <i class="material-icons">share</i>
                                            <span>Share</span> -->
                                        </a>
                                    </li>
                                </ul>

                            </div>
                        </div>
                    </div>

                </div>
            </div>
            </div>
        @endforeach
    @else
        <p>No articles yet</p>
    @endif
    {{$articles->links()}}
@endsection
