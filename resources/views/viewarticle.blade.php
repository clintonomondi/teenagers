@extends('layouts.app')

@section('content')
    @include('includes.message')
    @include('sweet::alert')
    <div class="container-fluid">
        <div class="row clearfix">
            
            <div class="col-xs-12 col-sm-12">
                <div class="card">
                        <div class="header">
                                <h2>
                                    {{$article->title}} <small>{{$article->category}}  {{$article->created_at->diffForHumans()}}</small>
                                </h2>
                    <div class="body">
      <p> {!!$article->body!!}</p>
<hr>
                            @foreach ($article->comment as $comment)
                                    <div class="panel panel-default panel-post">
                                        <div class="panel-heading">
                                            <div class="media">
                                                <div class="media-left">
                                                    <a href="#">
                                                        <img src="/storage/avatars/{{ $comment->user->avatar}}" />
                                                    </a>
                                                </div>
                                                <div class="media-body">
                                                    <h4 class="media-heading">
                                                        <a href="#">{{ $comment->user->name }}</a>
                                                    </h4>
                                                   {{$comment->created_at->diffForHumans()}}
                                                </div>
                                            </div>
                                        </div>
                                        <div class="panel-body">
                                            <div class="post">
                                                <div class="post-heading">
                                                    <p>{!!$comment->comment !!}</p>
                                                </div>
                                                <div class="post-content">
                                                    
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    @endforeach
                                    <div class="panel panel-default panel-post">
                                            <div class="panel-heading">
                                    <div class="panel-footer">
                                            <ul>
                                                <li>
                                                    <a href="#">
                                                        {{--<i class="material-icons">thumb_up</i>--}}
                                                        {{--<span>12 Likes</span>--}}
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="#">
                                                        <i class="material-icons">comment</i>
                                                        <span>{{ $article->comment->count() }} Comments</span>
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="#">
                                                        <i class="material-icons">share</i>
                                                        <span>Share</span>
                                                    </a>
                                                </li>
                                            </ul>
                                            <form  method="post" action="{{route('comment')}}">
                                                @csrf
                                                <div class="row clearfix">
                                                        <div class="col-lg-10">
                                            <div class="form-group">
                                                <div class="form-line">
                                                        <input type="text" value="{{Auth::user()->id}}" name="user_id" hidden>
                                                        <input type="text" value="{{$article->id}}" name="article_id" hidden>
                                                    <input type="text" class="form-control" placeholder="Type a comment" name="comment" required />
                                                </div>
                                            </div>
                                                        </div>

                                               <div class="col-lg-2">
                                            <div class="form-group">
                                                <div class="form-line">
                                                <button type="submit" class="btn bg-pink waves-effect">POST</button>
                                                </div>
                                            </div>
                                               </div>
                                            </form>
                                            </div>
                                        </div>
                                    </div>
                               
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
