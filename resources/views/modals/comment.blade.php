<!-- The Modal -->
<div class="modal fade" id="comment">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">

            <!-- Modal Header -->
            <div class="modal-header">
                <h4 class="modal-title">Write comment</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <!-- Modal body -->
            <div class="modal-body">
                <form  method="post" action="{{route('comment')}}">
                    @csrf

                    <div class="form-group">
                        <label for="comment">Body:</label>
                        <textarea class="form-control" rows="5" id="article-ckeditor" name="comment" required></textarea>
                    </div>
                    <input type="text" value="{{Auth::user()->id}}" name="user_id" hidden>
                    <input type="text" value="{{$article->id}}" name="article_id" hidden>
                    <div class="form-group form-check">

                    </div>
                    <button type="submit" class="btn btn-success mr-2 btn-sm">Post</button>
                </form>
            </div>

            <!-- Modal footer -->
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
            </div>

        </div>
    </div>
</div>