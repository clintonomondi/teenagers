 <!-- Default Size -->
 <div class="modal fade" id="editquiz" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="defaultModalLabel">Ask Question</h4>
                </div>
                <div class="modal-body">
                        <<form method="post" action="{{route('updatequiz',$quiz->id)}}">
                                @csrf
                                <div class="form-group">
                                        <div class="form-line">
                                    <label for="email">Title:</label>
                                    <input type="text" class="form-control"  name="title" placeholder="Title" value="{{$quiz->title}}" required>
                                </div>
                                </div>
                                <div class="form-group">
                                        <div class="form-line">
                                    <label for="comment">Body:</label>
                                    <textarea class="form-control" rows="10" id="article-ckeditor" name="body" required>{!!$quiz->body!!}</textarea>
                                </div>
                                </div>
                                <input type="text" name="user_id" value="{{Auth::user()->id}}" hidden>
                                <div class="form-group form-check">
            
                                </div>
                           
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn bg-pink waves-effect">Update</button>
                </form>
                    <button type="button" class="btn bg-red waves-effect" data-dismiss="modal">CLOSE</button>
                </div>
            </div>
        </div>
    </div>