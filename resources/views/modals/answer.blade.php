 <!-- Default Size -->
 <div class="modal fade" id="answer" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="defaultModalLabel">Answer Question</h4>
                </div>
                <div class="modal-body">
                        <form method="post" action="{{route('postanswer')}}">
                                @csrf
                                <div class="form-group">
                                        <div class="form-line">
                                    <label for="comment">Body:</label>
                                    <textarea class="form-control" rows="30" id="article-ckeditor" name="body" required></textarea>
                                </div>
                                </div>
                                <input type="text" name="question_id" value="{{$quiz->id}}" hidden>
                                <input type="text" name="status" value="Answered" hidden>
                                <input type="text" name="user" value="{{Auth::user()->name}}" hidden>
                                <div class="form-group form-check">
            
                                </div>
                           
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn bg-pink waves-effect">Submit</button>
                </form>
                    <button type="button" class="btn bg-red waves-effect" data-dismiss="modal">CLOSE</button>
                </div>
            </div>
        </div>
    </div>