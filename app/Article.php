<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Article extends Model
{
    protected $fillable=['category','title','body'];

    public  function comment(){
        return $this->hasMany(Comment::class);
    }
}
