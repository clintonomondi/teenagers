<?php

namespace App\Http\Controllers;

use App\Answer;
use App\Article;
use App\Question;
use App\User;
use Illuminate\Http\Request;
use Alert;

class AdminController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function users()
    {
        $users = User::orderBy('id', 'asc')->paginate(1000);
        return view('admin.users', compact('users'));
    }

    public function removeuser($id)
    {
        $member = User::find($id);
        $member->delete();
        Alert::success('User removed successfully', 'Success')->persistent("Ok");
        return redirect()->back();

    }

    public  function articles(){
        $articles=Article::orderBy('id','desc')->paginate(1000);
        return view('admin.articles',compact('articles'));
    }

    public  function addarticle(){
        return view('admin.addarticle');
    }

    public  function  postarticle(Request $request){
        $article=Article::create($request->all());
        Alert::success('New article added', 'Success')->persistent("Ok");
        return redirect()->back();
    }

    public  function  removearticle($id){
        $article=Article::find($id);
        $article->delete();
        Alert::success('An article removed', 'Success')->persistent("Ok");
        return redirect()->back();
    }

    public  function editarticle($id){
        $article=Article::find($id);
        return view('admin.editarticle',compact('article'));
    }

    public  function updatearticle(Request $request,$id){
        $article=Article::find($id);
        $article->update($request->all());
        Alert::success('An article has been updated', 'Success')->persistent("Ok");
        return redirect()->back();
    }

    public  function viewarticle($id){
        $article=Article::find($id);
        return view('admin.viewarticle',compact('article'));
    }

    public  function allarticles(){
        $articles=Article::orderBy('id','desc')->paginate(1000);
        return view('admin.allarticles',compact('articles'));
    }

    public  function questions(){
        $questions=Question::orderBy('id','desc')->paginate(1000);
        return view('admin.questions',compact('questions'));
    }

    public  function viewquiz($id){
        $quiz=Question::find($id);
        return view('admin.viewquiz',compact('quiz'));
    }

    public  function postanswer(Request $request){
        $id=$request->input('question_id');
        $status=$request->input('status');
        $quiz=Question::find($id);
        $quiz->status=$status;
        $quiz->save();
        $ans=Answer::create($request->all());
        Alert::success('Answer posted successfully', 'Success')->persistent("Ok");
        return redirect()->back();
    }
}
