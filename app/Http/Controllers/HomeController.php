<?php

namespace App\Http\Controllers;

use App\Article;
use App\Comment;
use App\Question;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Alert;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $users=User::count();
        $articles=Article::count();
        $pending=Question::where('status','Pending')->count();
        $comments=Comment::count();
        $mycomments=User::find(Auth::user()->id)->comment()->count();
        $mypending=User::find(Auth::user()->id)->question()->where('status','Pending')->count();
        return view('home',compact('users','articles','pending','comments','mycomments','mypending'));
    }

    public  function avater(){
        return view('avater');
    }

    public  function updateavarter(Request $request){
        $request->validate([
            'avatar' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:1999',
        ]);
        //get file name with extension
        $fileNameWithExt=$request->file('avatar')->getClientOriginalName();
        $filename=pathinfo($fileNameWithExt,PATHINFO_FILENAME);
        $extesion=$request->file('avatar')->getClientOriginalExtension();
        //filename to store
        $fileNameToStore=$filename.'_'.time().'.'.$extesion;
        //uploadimage
        $path=$request->file('avatar')->storeAs('/public/avatars',$fileNameToStore);

        $user=User::find(Auth::user()->id);
        $user->avatar=$fileNameToStore;
        $user->save();
        Alert::success('Profile pic updated successfully', 'Success')->persistent("Ok");
        return redirect()->back();
    }

    public  function account(){
        return view('account');
    }

    public  function updateaccount(Request $request,$id){
        $user=User::find($id);
        $user->update($request->all());

        Alert::success('Account updated successfully', 'Success')->persistent("Ok");
        return redirect()->back();
    }

    public  function  resetpass(Request $request){
        $this->validate($request, [
            'currentpass' => 'required|min:5',
            'newpass' => 'required|min:5',
            'repass' => 'required|min:5',
        ]);
        $currentpass = auth()->user()->password;
        if (!Hash::check($request['currentpass'], $currentpass)) {
            Alert::error('The entered current password is wrong', 'Error')->persistent("Ok");
            return redirect()->back();
        } elseif ($request->input('newpass') !== $request->input('repass')) {
            Alert::error('The new passwords do not match', 'Error')->persistent("Ok");
            return redirect()->back();
        }
        $id = auth()->user()->id;
        $currentUser = User::findOrFail($id);
        $currentUser->password = Hash::make($request->input('newpass'));
        $currentUser->save();
        Alert::success('Password updated successfully', 'Success')->persistent("Ok");
        return redirect()->back();
    }

    public  function allarticles(){
        $articles=Article::orderBy('id','desc')->paginate(1000);
        return view('allarticles',compact('articles'));
    }

    public  function viewarticle($id){
        $article=Article::find($id);
        $articlecount=Article::find($id)->comment()->count();
        return view('viewarticle',compact('article','articlecount'));
    }

   

    public  function comment(Request $request){
        $comment=Comment::create($request->all());
        Alert::success('Comment posted successfully', 'Success')->persistent("Ok");
        return redirect()->back();
    }

    public  function question(){
        $questions=User::find(Auth::user()->id)->question()->orderBy('id','desc')->paginate(20);
        return view('questions',compact('questions'));
    }

    public  function postquiz(Request $request){
        $quiz=Question::create($request->all());
        Alert::success('Question posted successfully', 'Success')->persistent("Ok");
        return redirect()->route('question');
    }

    public  function cancelquiz($id){
        $quiz=Question::find($id);
        $quiz->delete();
        Alert::success('Question cancelled successfully', 'Success')->persistent("Ok");
        return redirect()->back();
    }

    public  function viewquiz($id){
       $quiz=Question::find($id);
       return view('viewquiz',compact('quiz'));
}

public  function updatequiz(Request $request,$id){
        $quiz=Question:: find($id);
        $quiz->update($request->all());
    Alert::success('Question updated successfully', 'Success')->persistent("Ok");
    return redirect()->back();

}


public function search(Request $request){
    $q = $request->input('q');
    $articles = Article::where('id', 'LIKE', '%' . $q . '%')->orWhere('title', 'LIKE', '%' . $q . '%')->orWhere('category', 'LIKE', '%' . $q . '%')->orWhere('created_at', 'LIKE', '%' . $q . '%')->get();

        return view('result', compact('articles'));
}

}
