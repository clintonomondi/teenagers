<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Answer extends Model
{
    protected  $fillable=['question_id','body','user'];

    public  function question(){
        return $this->belongsTo(Question::class);
    }
}
