<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

//users
Route::get('/avater', 'HomeController@avater')->name('avater');
Route::post('/avater', 'HomeController@updateavarter')->name('updateavarter');
Route::get('/account', 'HomeController@account')->name('account');
Route::post('/account/update/{id}', 'HomeController@updateaccount')->name('updateaccount');
Route::post('/password', 'HomeController@resetpass')->name('resetpass');
Route::get('/user/article/all', 'HomeController@allarticles')->name('user.allarticles');
Route::get('/user/article/orderall', 'HomeController@orderall')->name('orderall');
Route::get('/user/article/view/{id}', 'HomeController@viewarticle')->name('user.viewarticle');

Route::get('/user/questions', 'HomeController@question')->name('question');
Route::post('/user/questions/post', 'HomeController@postquiz')->name('postquiz');
Route::get('/user/questions/cancel/{id}', 'HomeController@cancelquiz')->name('cancelquiz');
Route::get('/user/view/quiz/{id}', 'HomeController@viewquiz')->name('user.viewquiz');
Route::post('/user/update/quiz/{id}', 'HomeController@updatequiz')->name('updatequiz');
Route::post('/search/result', 'HomeController@search')->name('search');

//admin tnings
Route::get('/users', 'AdminController@users')->name('users');
Route::get('/user/remove/{id}', 'AdminController@removeuser')->name('removeuser');
Route::get('/articles', 'AdminController@articles')->name('articles');
Route::get('/article/add', 'AdminController@addarticle')->name('addarticle');
Route::post('/article/add', 'AdminController@postarticle')->name('postarticle');
Route::get('/article/remove/{id}', 'AdminController@removearticle')->name('removearticle');
Route::get('/article/edit/{id}', 'AdminController@editarticle')->name('editarticle');
Route::post('/article/update/{id}', 'AdminController@updatearticle')->name('updatearticle');
Route::get('/article/view/{id}', 'AdminController@viewarticle')->name('viewarticle');
Route::get('/article/all', 'AdminController@allarticles')->name('allarticles');
Route::get('/admin/questions', 'AdminController@questions')->name('questions');
Route::get('/admin/view/question/{id}', 'AdminController@viewquiz')->name('admin.viewquiz');
Route::post('/admin/answer/post', 'AdminController@postanswer')->name('postanswer');

Route::post('/comment', 'HomeController@comment')->name('comment');

